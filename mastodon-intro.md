## ทำความรู้จัก Mastodon(/Fediverse) กันเสียก่อน:

[วิธีสร้างบัญชีและเริ่มใช้ Mastodon ตั้งแต่เริ่ม ฉบับภาษาไทย](https://www.telecomlover.com/2020/07/05/mastodon-guide-thai/)\
\*ปัจจุบัน Mastodon ได้อัปเดตเวอร์ชันใหม่ หน้าตาของเมนูอาจต่างไปจากบทความ

## แอปพลิเคชันสำหรับสมาร์ตโฟน

- **แอปฯ ที่ผลิตโดย Mastodon เอง**\
  [สำหรับ Android](https://play.google.com/store/apps/details?id=org.joinmastodon.android)\
  [สำหรับ iOS](https://apps.apple.com/us/app/mastodon-for-iphone/id1571998974)
- **แอปฯ ที่ไม่ได้ผลิตโดย Mastodon**\
  Android: [Tusky](https://play.google.com/store/apps/details?id=com.keylesspalace.tusky) \| [Subway Tooter](https://play.google.com/store/apps/details?id=jp.juggler.subwaytooter) \| [Fedilab](https://play.google.com/store/apps/details?id=app.fedilab.android)\
  iOS: [Toot!](https://itunes.apple.com/app/toot/id1229021451) \| [Mast](https://apps.apple.com/us/app/mast-for-mastodon/id1437429129)

## เซิร์ฟเวอร์ (หรือ instance) แนะนำที่นอกจาก mastodon.in.th (ตอนนี้เต็มจนปิดรับสมัครไปแล้ว)

Fediverse มีอิสระสูง คุณสามารถทำได้แม้กระทั่งสร้างเซิร์ฟเวอร์มาใช้งานเพียงแค่คน ๆ เดียว (จะปิดหรือเปิดให้บุคคลภายนอกสมัครบัญชีก็ได้) โดยที่ไม่ต้องกลัวว่าจะมีใครที่ไหนมาแบนบัญชีเรา\
<https://mastodon.cloud/> ของญี่ปุ่น (Sujitech, LLC.)\
<https://pawoo.net/> ของญี่ปุ่น เหมาะสำหรับลงภาพวาด\
<https://mstdn.party/>\
<https://home.social/>\
<https://ohai.social/>\
~~https://mas.to/~~ ปิดรับสมัคร\
<https://mstdn.social/>\
<https://masto.ai/>\
<https://social.vivaldi.net/> ของ Vivaldi ต้องสมัครบัญชี Vivaldi ด้วย

### ต้องรอผู้ดูแลยืนยัน

<https://libretooth.gr/>

### สำหรับโอตาคุ (ผู้เขียนเรียก Weeb)

~~https://ani.work/~~ ปิดรับสมัครชั่วคราว\
<https://social.silicon.moe/> สำรอง

## รู้ไหม? Fediverse ไม่ได้มีแค่ Mastodon นะ!

- **Pleroma** เหมือน Mastodon แต่โพสต์ตัวหนังสือได้มากกว่า ตอบกลับโพสต์คนอื่นด้วย emoji ได้ด้วย\
  [แอปฯ (สำหรับ Android) (ต้องลงผ่านไฟล์ .apk)](https://f-droid.org/en/packages/su.xash.husky/)

  <https://miraiverse.xyz/> ของไทย\
  <https://capybara.social/> คาปิบาร่า ของไทย\
  <https://stereophonic.space/>\
  <https://blob.cat/> น้องแมววว\
  <https://freespeechextremist.com/>\
  <https://social.076.ne.jp/> Weeb + ของญี่ปุ่น

- **Misskey UI** น่ารัก ลูกเล่นหลากหลาย\
  [แอปฯ (สำหรับ Android)](https://play.google.com/store/apps/details?id=jp.panta.misskeyandroidclient)

  <https://misskey.io/>\
  <https://misskey.social/>\
  <https://sushi.ski/> ซูชิ

- **Friendica** หน้าตาเหมือน Facebook\
  <https://friendica.me/>\
  <https://nerdica.net/>

- ฯลฯ

\*ข้อมูลเกี่ยวกับเซิร์ฟเวอร์ Fediverse สำหรับคนไทย: <https://fediverse.in.th/>

## รายชื่อบัญชี Mastodon(/Fediverse) ที่แนะนำให้ติดตาม

ต้องเข้าใจว่า Mastodon(/Fediverse) (ยัง)ไม่มีระบบแนะนำให้ติดตาม หรือสุ่มโพสต์ของคนอื่นที่คุณอาจสนใจ ถ้าเข้ามาแล้วมันดูเงียบเหงา เราก็ติดตามบัญชีของคนอื่นไว้เยอะ ๆ\
เพื่อความสะดวก ผมทำไฟล์ CSV สำหรับติดตามหลายบัญชีพร้อมกันเอาไว้ด้วย: [ดาวน์โหลดได้ที่นี่](https://drive.google.com/file/d/1tuDbFksRME-yi85SIWtMrD_NYU1UGjnj/view?usp=share_link)\
[วิธีเข้าถึงเนื้อหาจากเซิร์ฟเวอร์(instance) อื่น และติดตามหลายบัญชีพร้อมกันด้วยการอัพโหลดไฟล์ CSV](https://youtu.be/wFxiG4Y38zU)

### คนไทย

| URL                                         | Note                                                                |
| ------------------------------------------- | ------------------------------------------------------------------- |
| https://xxx.azyobuzi.net/kafaedam           | กาแฟดำ                                                              |
| https://capybara.social/papipupepo          | คาปิบาร่าง่วงนอน                                                    |
| https://capybara.social/users/yuzu_capybara | คาปิบาร่า                                                           |
| https://die-partei.social/@hackernews       | บอต                                                                 |
| https://mastodon.art/@proton_plus           |                                                                     |
| https://mastodon.in.th/@aintmyfltt          |                                                                     |
| https://mastodon.in.th/@aosora              |                                                                     |
| https://mastodon.in.th/@astia               |                                                                     |
| https://mastodon.in.th/@aunthedpun          |                                                                     |
| https://mastodon.in.th/@AZI1MOV             |                                                                     |
| https://mastodon.in.th/@bact                |                                                                     |
| https://mastodon.in.th/@bakamute            |                                                                     |
| https://mastodon.in.th/@boon                |                                                                     |
| https://mastodon.in.th/@boypotter           |                                                                     |
| https://mastodon.in.th/@chai_siris          |                                                                     |
| https://mastodon.in.th/@cwt                 |                                                                     |
| https://mastodon.in.th/@darkerkuro          |                                                                     |
| https://mastodon.in.th/@defdef              |                                                                     |
| https://mastodon.in.th/@densin              |                                                                     |
| https://mastodon.in.th/@disgezhtle          |                                                                     |
| https://mastodon.in.th/@doz                 |                                                                     |
| https://mastodon.in.th/@dunstinvyne         |                                                                     |
| https://mastodon.in.th/@Eyri                |                                                                     |
| https://mastodon.in.th/@fit_for_fine        |                                                                     |
| https://mastodon.in.th/@gagaboog            |                                                                     |
| https://mastodon.in.th/@grainbrid           |                                                                     |
| https://mastodon.in.th/@healthmenowth       | สุขภาพ                                                              |
| https://mastodon.in.th/@hopperosfh          |                                                                     |
| https://mastodon.in.th/@Hyojokajung         |                                                                     |
| https://mastodon.in.th/@itcountry           |                                                                     |
| https://mastodon.in.th/@ivader              |                                                                     |
| https://mastodon.in.th/@jirayu              |                                                                     |
| https://mastodon.in.th/@joe                 |                                                                     |
| https://mastodon.in.th/@Joez19              |                                                                     |
| https://mastodon.in.th/@jungbi              |                                                                     |
| https://mastodon.in.th/@kimbab994           |                                                                     |
| https://mastodon.in.th/@ktst                |                                                                     |
| https://mastodon.in.th/@kukkik              |                                                                     |
| https://mastodon.in.th/@littlebirbmame      |                                                                     |
| https://mastodon.in.th/@lluu                |                                                                     |
| https://mastodon.in.th/@mina                |                                                                     |
| https://mastodon.in.th/@mishari             |                                                                     |
| https://mastodon.in.th/@MiuniblueG          |                                                                     |
| https://mastodon.in.th/@Mosquito            | จารยุง                                                              |
| https://mastodon.in.th/@moui                |                                                                     |
| https://mastodon.in.th/@nabsib              |                                                                     |
| https://mastodon.in.th/@nfo                 |                                                                     |
| https://mastodon.in.th/@nicknznick          |                                                                     |
| https://mastodon.in.th/@NiyayZAP            | นิยายแซ่บ อย่าลืมไปแวะเวียน & อุดหนุนนิยายที่เค้าเขียน              |
| https://mastodon.in.th/@Nuax4qz             |                                                                     |
| https://mastodon.in.th/@onxrynzo            |                                                                     |
| https://mastodon.in.th/@pickyzz             |                                                                     |
| https://mastodon.in.th/@pompoko35           |                                                                     |
| https://mastodon.in.th/@ppnplus             | คนเขียน วิธีสร้างบัญชีและเริ่มใช้ Mastodon ตั้งแต่เริ่ม ฉบับภาษาไทย |
| https://mastodon.in.th/@rinx                |                                                                     |
| https://mastodon.in.th/@ronliniix           |                                                                     |
| https://mastodon.in.th/@sarsi               |                                                                     |
| https://mastodon.in.th/@sirn                |                                                                     |
| https://mastodon.in.th/@Sophiaya            |                                                                     |
| https://mastodon.in.th/@tck                 |                                                                     |
| https://mastodon.in.th/@thaitechfeed        | บอต                                                                 |
| https://mastodon.in.th/@theera              |                                                                     |
| https://mastodon.in.th/@thematterco         | บอต The MATTER                                                      |
| https://mastodon.in.th/@thep                |                                                                     |
| https://mastodon.in.th/@veer66              | บอตวี                                                               |
| https://mastodon.in.th/@wari                |                                                                     |
| https://mastodon.in.th/@whs                 |                                                                     |
| https://mastodon.in.th/@wlstgs              |                                                                     |
| https://mastodon.in.th/@yourboyxjes         |                                                                     |
| https://mastodon.online/@dysem              |                                                                     |
| https://mastodon.sdf.org/@bunyanee_thongdee |                                                                     |
| https://mastodon.social/@dej45              | pizad_sura                                                          |
| https://mastodon.social/@lazywasabi         |                                                                     |
| https://miraiverse.xyz/users/kouunnjiTH     |                                                                     |
| https://miraiverse.xyz/users/sukino         | สุกีโนะ คนเขียน https://fediverse.in.th/                            |
| https://miraiverse.xyz/users/wlstgs         |                                                                     |
| https://mstdn.fediverse.asia/@boon          |                                                                     |
| https://mstdn.social/@ryn_rinn              |                                                                     |
| https://mtd.bashell.com/@cwt                |                                                                     |
| https://pleroma.in.th/@lazarus              | นิยาย                                                               |
| https://viisphere.me/@vii                   |                                                                     |

### ไม่ใช่คนไทย

| URL                                             | Note                       |
| ----------------------------------------------- | -------------------------- |
| https://botsin.space/@catsnkittens              | บอตแมว                     |
| https://fedibird.com/@ymd                       | แมว                        |
| https://kolektiva.social/@AnarComNet            | Anarcho-communism          |
| https://mastodon.ar.al/@aral                    |                            |
| https://mastodon.art/@comfyfinn                 | Pixel art                  |
| https://mastodon.in.th/@mekongtandem            |                            |
| https://mastodon.social/@Gargron                | ศาสดาผู้ให้กำเนิด Mastodon |
| https://mastodon.social/@GoatsLive              | แพะ                        |
| https://mastodon.social/@youronlyone            |                            |
| https://mastodont.cat/@fediverse                | บอต                        |
| https://mstdn.jp/@salutora                      | น้องหมา อูเมะคิจิ          |
| https://mstdn.social/@stux                      |                            |
| https://social.network.europa.eu/@EU_Commission | EU                         |
